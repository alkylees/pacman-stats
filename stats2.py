import parse_debug_files
import typer
import sys
import time

app = typer.Typer()

@app.command()
def generate_stats(
    player: str = typer.Argument(..., help="Name of the player"),
    map_path: str = typer.Argument(..., help="Path to the map file"),
    nb_runs: int = typer.Argument(..., help="Number of runs"),
    mode : str = typer.Argument(..., help="Mode of the game")
):
    """Generate stats for a given executable and map file"""

    score_max = 0
    score_min = 0
    score_avg = 0

    nb_iter_max = 0
    nb_iter_min = 0
    nb_iter_avg = 0

    nb_win = 0
    nb_lose = 0


    nb_bumped = 0
    nb_eaten = 0
    nb_unknow_death = 0

    complition_max = 0
    complition_min = 0
    complition_avg = 0

    start_time = time.time()

    for i in range(nb_runs):
        # start by running the program 
        completedprocess = parse_debug_files.run(player, map_path,debug="on", mode=mode)

        score = parse_debug_files.get_score("tests/debug.txt")
        nb_iter = parse_debug_files.get_number_of_iterations("tests/debug.txt", map_path)
        remaining_points = parse_debug_files.get_remaining_points("tests/debug.txt", map_path)
        initial_points = parse_debug_files.get_initial_points("tests/debug.txt", map_path)

        if score > score_max:
            score_max = score
        if score < score_min:
            score_min = score
        score_avg += score

        if nb_iter > nb_iter_max:
            nb_iter_max = nb_iter
        if nb_iter < nb_iter_min:
            nb_iter_min = nb_iter
        nb_iter_avg += nb_iter

        if remaining_points == 0:
            nb_win += 1
        else:
            nb_lose += 1

        complition = (initial_points - remaining_points) / initial_points
        if complition > complition_max:
            complition_max = complition
        if complition < complition_min:
            complition_min = complition
        complition_avg += complition

        
        bumbstring = "RIP: you bumped into a wall!"
        eatenstring = "RIP: you have just been eaten by a ghost!"

        if bumbstring in completedprocess.stdout.decode("utf-8"):
            nb_bumped += 1
        elif eatenstring in completedprocess.stdout.decode("utf-8") :
            nb_eaten += 1
        else:
            nb_unknow_death += 1


        progress_bar(i, nb_runs, message="Score: " + str(score) + " Iterations: " + str(nb_iter) + " Finished at "+ str(complition) + "%")
    
    print("\nScore: max: " + str(score_max) + ", min: " + str(score_min) + ", avg: " + str(score_avg / nb_runs))
    print("Number of iterations: max: " + str(nb_iter_max) + ", min: " + str(nb_iter_min) + ", avg: " + str(nb_iter_avg / nb_runs))
    print("Complition: max: " + str(complition_max) + ", min: " + str(complition_min) + ", avg: " + str(complition_avg / nb_runs))
    print("Ratio: " + color_ratio_number(nb_win / nb_runs) + "%")
    print("Bumped on lose: " + color_bumbed_on_lose(nb_bumped, nb_lose) + "%")
    print("Eaten on lose: " + str(nb_eaten / nb_lose * 100) + "%")
    print("Unknow death on lose: " + str(nb_unknow_death / nb_lose * 100) + "%")
    print("Time: " + str(time.time() - start_time))


        

    






def progress_bar(current, total, bar_length=20, message=""):
    percent = float(current) * 100 / total
    arrow = '-' * int(percent / 100 * bar_length - 1) + '>'
    spaces = ' ' * (bar_length - len(arrow))

    sys.stdout.write("\rProgress [{0}] {1}% {2}".format(arrow + spaces, color_progress_bar(int(percent)), message))
    sys.stdout.flush()

def color_ratio_number(ratio):
    if ratio > 0.5:
        return typer.style(str(ratio), fg=typer.colors.GREEN)
    else:
        return typer.style(str(ratio), fg=typer.colors.RED)

def color_bumbed_on_lose(bumped, lose):
    if bumped / lose < 0.5:
        return typer.style(str(bumped / lose), fg=typer.colors.GREEN)
    else:
        return typer.style(str(bumped / lose), fg=typer.colors.RED)

def color_progress_bar(progress):

    # 3 levels of progress bar
    # from red to green
    # 0.33 -> 0.66 -> 1
    if progress > 66:
        return typer.style(str(progress), fg=typer.colors.GREEN)
    elif progress > 33:
        return typer.style(str(progress), fg=typer.colors.YELLOW)
    else:
        return typer.style(str(progress), fg=typer.colors.RED)


if __name__ == "__main__":    
    app()
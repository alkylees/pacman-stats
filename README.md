# Pacman stats

Vous permet de faire des statistiques sur votre programme de pacman ansi que des tests unitaires.

Avant utilisation, installez la librarie python `Typer` et `pytest`. Lancez donc la commande dans le terminal:

```bash
pip3 install typer pytest
```


## Utilisation du script de statistiques:

```bash
python3 stats.py [path_du_binaire] [path_de_la_map] [nombre_de_parties] [mode_de_jeu]
```


## Tests unitaires:

Dans un fichiers `tests_pacman.py` vous pouvez faire des tests unitaires sur votre programme. Par exemple, vous pouvez vérifier que pac man ne meurt pas au premier tour de jeu.

```python
import unittest
import threading

from parse_debug_files import *

LEVELS_MAPS_PATHS = ["levels/level1.map", "levels/level2.map", "levels/level3.map"]
EXECUTABLE_PATH = "bin/pacman_player"
MAX_TIMEOUT_SECONDS = 5
REPETITIONS = 5

class TestPacmanStats(unittest.TestCase):
        
    def test_not_deat_at_first_iteration(self):
        number_of_iterations = []
        for map_path in LEVELS_MAPS_PATHS:
            completedprocess = run(EXECUTABLE_PATH, map_path)
            number_of_iterations.append(get_number_of_iterations("tests/debug.txt", map_path))
        
        # all the number of iterations should be greater than 1
        
        self.assertTrue(all([i > 1 for i in number_of_iterations]))

if __name__ == '__main__':
    unittest.main()
```

Dans le fichier `parse_debug_files.py` vous avez plusieurs méthodes que vous pouvez utiliser afin de créer vos tests. Les fonctions sont documentées dans le fichier.

Pour lancer l'exécutions des tests, lancez la commande:

```bash
pytest tests_pacman.py
```


# Attention
stats.py :
    - Si le fichier de debug est vide, on considere que c'est une victoire. --> A changer
import re
import os
import subprocess
import time
import sys

class MapNotFoundError(Exception):
    pass

class EmptyDebugFileError(Exception):
    pass

class coordinates():
    """ Object to represent a coordinate on a map
    """    
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        return "x: " + str(self.x) + ", y: " + str(self.y)

    def __repr__(self):
        return "< " + self.__str__() + " >"

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y

    def __ne__(self, other):
        return not self.__eq__(other)

def print_map(map):
    """Print the map in the console

    Args:
        map (list[list]): The map to print as a list of list
    """    
    for l in map:
        print("".join(l))

def place_coordinates(x,y,file):
    """Place a "x" on the map at the given coordinates

    Args:
        x (int): X coordinate of the point
        y (int): Y coordinate of the point
        file (str): The path to the map file
    """    """
        
    """
    map = []
    with open(file, "r") as f:
        content = f.read()
        map = content.split("\n")
    
    i = 0
    while i < len(map) - 1:
        map[i] = [c for c in map[i]]
        i+=1

    map[y][x] = "x"
    print_map(map)

    print("X size:", len(map[0]))
    print("Y size:", len(map))

def get_map_size(file):
    """Get the size of the map

    Args:
        file (str): The path to the map file

    Returns:
        coordinates: The size of the map as a coordinates object (x,y)
    """
    map = []
    with open(file, "r") as f:
        content = f.read()
        map = content.split("\n")
    
    i = 0
    while i < len(map) - 1:
        map[i] = [c for c in map[i]]
        i+=1

    return coordinates(len(map[0]), len(map))

def search_maps(debug_file_path, map_path):
    """Search all the maps in the debug file
        and return a list of the maps
        In a map, a wall is represented by a "*"
        So we can search for the "*" in the debug file
        other characters are not important here

    Args:
        debug_file_path (str): The path to the debug file
        map_path (str): The path to the map file

    Returns:
        list[str]: A list of the maps found in the debug file
    """
    # Read the map file

    with open(map_path, "r") as mapfile:
        map_file_content = mapfile.read()
    
    # Transform it to a partern to match with regex
    map_detection_patern = [c for c in map_file_content]
    for i in range(0, len(map_detection_patern)):
        if map_detection_patern[i] != "*" and map_detection_patern[i] != "\n":
            map_detection_patern[i] = "."
    map_detection_patern = "".join(map_detection_patern)

    map_detection_patern = map_detection_patern.replace(r"*", r"\*")

    #print(map_detection_patern)

    #return map_detection_patern

    # Read the debug file
    with open(debug_file_path, "r") as debugfile:
        debug_file_content = debugfile.read()
        matched_maps = re.findall(map_detection_patern, debug_file_content)
        return matched_maps

def generate_map_detection_patern(debug_file_path, map_path):
    """Generate a regex pattern to detect the map in the debug file

    Args:
        debug_file_path (str): The path to the debug file
        map_path (str): The path to the map file

    Returns:
        str: The regex pattern to detect the map in the debug file
    """    
    with open(map_path, "r") as mapfile:
        map_file_content = mapfile.read()
    
    # Transform it to a partern to match with regex
    map_detection_patern = [c for c in map_file_content]
    for i in range(0, len(map_detection_patern)):
        if map_detection_patern[i] != "*" and map_detection_patern[i] != "\n":
            map_detection_patern[i] = "."
    map_detection_patern = "".join(map_detection_patern)

    map_detection_patern = map_detection_patern.replace(r"*", r"\*")
    return map_detection_patern

def read_pacman_positions(debugfile_path, map_path):
    """Read the pacman position from the given file
        where pacman is represented by a "@" (or "_" when dead) on multiple maps represented in the file

    Args:
        debugfile_path (str): The path to the debug file
        map_path (str): The path to the map file

    Returns:
        list[coordinates]: A list of the pacman positions on each map as coordinates objects (x,y)
    """    
    positions = []
    map_size = get_map_size(map_path)

    matched_maps = search_maps(debugfile_path, map_path)

    # determine the pacman position on each map
    for m in matched_maps :
        
        index_in_string =  m.find("@")
        index_in_string = index_in_string if index_in_string != -1 else m.find("_") # if pacman is dead, it's represented by "_"

        positions.append(coordinates(index_in_string % map_size.x, index_in_string // map_size.x))

    return positions

def replay_game(debug_file_path, map_path):
    """Replay the game

    Args:
        debug_file_path (str): The path to the debug file
        map_path (str): The path to the map file
    """    
    frames = search_maps(debug_file_path, map_path)
    # print each frame with flush and delay
    for f in frames:
        sys.stdout.write("\r\n"+f)
        time.sleep(0.1)
        sys.stdout.flush()

def get_number_of_iterations(debug_file_path, map_path):
    """Get the number of iterations

    Args:
        debug_file_path (str): The path to the debug file
        map_path (str): The path to the map file

    Returns:
        int: The number of movements made by Pacman
    """
    return len(search_maps(debug_file_path, map_path))

def get_score(debug_file_path):
    """Get the max score

    Args:
        debug_file_path (str): The path to the debug file

    Returns:
        int: The max score of the game
    """    # search "score" in the debug file with regex
    with open(debug_file_path, "r") as debugfile:
        debug_file_content = debugfile.read()
        score = re.findall("SCORE: (?P<score>[0-9]+)", debug_file_content)[-1] or 0
        return int(score)

def get_remaining_points(debug_file_path, map_path):
    """Get the remaining points
        a point is represented by a "." or a "O" on the map

    Args:
        debug_file_path (str): The path to the debug file
        map_path (str): The path to the map file

    Raises:
        MapNotFoundError: If the map is not found in the debug file

    Returns:
        int: The numver of remaining points
    """    
    # search the remaining points in the debug file with regex
    matched_maps = search_maps(debug_file_path, map_path)
    if len(matched_maps) > 0:
        last_map = matched_maps[-1]
        
        end_points = len(re.findall(r"\.", last_map)) + len(re.findall("O", last_map)) * 10
        return int(end_points)

    else:
        print(debug_file_path, map_path)
        #raise MapNotFoundError()

def get_initial_points(debug_file_path, map_path):
    """Get the initial points
        a point is represented by a "." or a "O" on the map

    Args:
        debug_file_path (str): The path to the debug file
        map_path (str): The path to the map file

    Raises:
        MapNotFoundError: If the map is not found in the debug file

    Returns:
        int: The number of initial points
    """    
    # search the initial points in the debug file with regex
    matched_maps = search_maps(debug_file_path, map_path)
    if len(matched_maps) > 0:
        first_map = matched_maps[0]
        
        start_points = len(re.findall(r"\.", first_map)) + len(re.findall("O", first_map)) * 10
        return int(start_points)

    else:
        print(debug_file_path, map_path)
        #raise MapNotFoundError()

def run(executable_path, map_path, debug="on", display="color", delay=10, mode="easy", output_file_path="tests/debug.txt"):
    """Run the executable
        Usage of the executable: pacman [-debug on/off] [-display color/bw] [-delay integer] [-mode easy/original] level_file

    Args:
        executable_path (str): The path to the executable file
        map_path (str): The path to the map file
        debug (str, optional): Debug mode. Defaults to "on".
        display (str, optional): Defaults to "color".
        delay (int, optional): Defaults to 10.
        mode (str, optional): Defaults to "easy".
        output_file_path (str, optional): Defaults to "tests/debug.txt".

    Returns:
        subprocess.CompletedProcess: The result of the execution
    """
    completedprocess =  subprocess.run([executable_path, "-debug", debug, "-display", display, "-delay", str(delay), "-mode", mode, map_path], capture_output=True)
    
    with open(output_file_path, "w") as f:
        if completedprocess.stdout.decode("utf-8") != "":
            f.write(completedprocess.stdout.decode("utf-8"))
            f.close()
        else:
            pass
            #raise EmptyDebugFileError()
    return completedprocess


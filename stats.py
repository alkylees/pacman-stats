import os
import typer
import time
import subprocess
import sys
import re
import parse_debug_files

app = typer.Typer()


def progress_bar(current, total, bar_length=20, message=""):
    percent = float(current) * 100 / total
    arrow = '-' * int(percent / 100 * bar_length - 1) + '>'
    spaces = ' ' * (bar_length - len(arrow))

    sys.stdout.write("\rProgress [{0}] {1}% {2}".format(arrow + spaces, color_progress_bar(int(percent)), message))
    sys.stdout.flush()

@app.command()
def generate_stats(
    player: str = typer.Argument(..., help="Name of the player"),
    map_path: str = typer.Argument(..., help="Path to the map file"),
    nb_runs: int = typer.Argument(..., help="Number of runs"),
    mode : str = typer.Argument(..., help="Mode of the game"),
    # compilation: bool = typer.Option(False, help="Should the program be compiled before running it ?")
    ):
    """Generate stats for a given executable and map file"""

    score_max = 0
    score_min = 0
    score_avg = 0

    time_max = 0
    time_min = 0
    time_avg = 0

    nb_iter_max = 0
    nb_iter_min = 0
    nb_iter_avg = 0

    nm_win = 0
    nm_lose = 0
    ratio = 0

    nb_bumped = 0
    nb_eaten = 0
    nb_dump = 0

    complition_max = 0
    complition_min = 0
    complition_avg = 0

    # if compilation:
    #     print(f"Compiling {player}.c ...")
    #     # Compilation string: gcc -Wall -g -O0 -o {player} pacman.o {player}.c -lm
    #     os.system(f"gcc -Wall -g -O0 -o {player} pacman.o {player}.c -lm")
    #     print(f"Compilation done !")

    print(f"Executing {player} on {map_path} {nb_runs} times in {mode} mode")

    nb_dots_map = open(map_path, "r").read().count(".")

    for i in range(nb_runs):
        start_time = time.time()
        cmd = f"{player} -debug on -delay 1 -mode {mode} {map_path} > debug.txt"
        a = os.system(cmd)
        end_time = time.time()
        
        elapsed_time = end_time - start_time


        # wait for the file to be written
        # the random buffer will not be used 2 times
        if elapsed_time < 0.1:
            time.sleep(0.2)

        with open("debug.txt", "r") as f:
            content = f.read()
            sc = re.findall(r"SCORE: (\d+)", content)
            nb_iter = len(sc) # number of iterations, nombre de fois où le score est affiché
            score = 0 if nb_iter == 0 else int(sc[-1]) # Score final de l'IA
            win = 1 if not "RIP" in content else 0 # 1 si l'IA a gagné, 0 sinon
            nb_bumped += 1 if "bumped" in content else 0
            nb_eaten  += 1 if "eaten" in content else 0
            pr = re.findall(r"points_remaining: (\d+)", content)
            complition = int(pr[-1]) / int(pr[0]) if len(pr) > 0 else 0


        if score > score_max:
            score_max = score
        if score < score_min:
            score_min = score
        score_avg += score

        if elapsed_time > time_max:
            time_max = elapsed_time
        if elapsed_time < time_min:
            time_min = elapsed_time
        time_avg += elapsed_time

        if nb_iter > nb_iter_max:
            nb_iter_max = nb_iter
        if nb_iter < nb_iter_min:
            nb_iter_min = nb_iter
        nb_iter_avg += nb_iter

        if complition > complition_max:
            complition_max = complition
        if complition < complition_min:
            complition_min = complition
        complition_avg += complition

        if win == 1:
            nm_win += 1
        else:
            nm_lose += 1
    
        # print a progress bar
        progress_bar(i+1, nb_runs, message=f"{i+1}/{nb_runs}")

    print("""
    
    Score max: {}
    Score min: {}
    Score avg: {}

    Nb iter max: {}
    Nb iter min: {}
    Nb iter avg: {}

    Number of wins: {}
    Number of loses: {}
    Ratio: {}%

    Bumped: {}
    Eatent: {}

    Bumped/lose: {}
    Eatent/lose: {}


    """.format(
        score_max,
        score_min,
        score_avg / nb_runs,
        nb_iter_max,
        nb_iter_min,
        nb_iter_avg / nb_runs,
        nm_win,
        nm_lose,
        color_ratio_number(nm_win / nb_runs * 100),
        nb_bumped,
        nb_eaten,
        color_bumbed_on_lose(nb_bumped, nm_lose) if nm_lose > 0 else typer.style("Pas de loose ici", fg=typer.colors.GREEN),
        nb_eaten/nm_lose if nm_lose > 0 else typer.style("Pas de loose ici", fg=typer.colors.GREEN),
        ))


def color_ratio_number(ratio):
    if ratio > 0.5:
        return typer.style(str(ratio), fg=typer.colors.GREEN)
    else:
        return typer.style(str(ratio), fg=typer.colors.RED)

def color_bumbed_on_lose(bumped, lose):
    if bumped / lose < 0.5:
        return typer.style(str(bumped / lose), fg=typer.colors.GREEN)
    else:
        return typer.style(str(bumped / lose), fg=typer.colors.RED)

def color_progress_bar(progress):

    # 3 levels of progress bar
    # from red to green
    # 0.33 -> 0.66 -> 1
    if progress > 66:
        return typer.style(str(progress), fg=typer.colors.GREEN)
    elif progress > 33:
        return typer.style(str(progress), fg=typer.colors.YELLOW)
    else:
        return typer.style(str(progress), fg=typer.colors.RED)


if __name__ == "__main__":
    app()